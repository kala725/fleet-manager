@extends('main')
@section('style')
    <style>
        .table-sm {
            width: 50%;
            margin: 0 auto;
        }
    </style>
@endsection
@section('content')
    <h3>Employee Hierarchy: </h3>
    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Role</th>
            </tr>
        </thead>
        <tbody>
        @foreach(array_reverse($hierarchy) as $emp)
            <tr>
                <td scope="col">{{ $emp['name'] }}</td>
                <td scope="col">{{ $emp['role'] }}</td>
            </tr>
            @if( !$loop->last)
                <tr class="text-center">
                    <td scope="col"><span class="glyphicon glyphicon-sort"></span></td>
                    <td scope="col"></td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>

@endsection