@extends('main')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Fleet Management App
            </div>

            <div class="links">
                <a href="/employee/">List All Employees</a>
                <a href="/employee/create">Create Employees</a>
                <a href="/employee/bonus">Distribute Bonus / List top employees</a>
            </div>
        </div>
    </div>
@endsection
