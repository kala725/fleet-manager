<?php

use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('locations')
            ->insert([
                ['name' => 'Bengaluru', 'slug' => 'blr'],
                ['name' => 'Mumbai', 'slug' => 'mum']
            ]);
    }
}
