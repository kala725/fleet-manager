<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Illuminate\Support\Facades\DB::table('employees')
            ->insert([
                ['id' => 1, 'name' => 'City Manager', 'salary' => 20000, 'rating' => 5, 'role_id' => 1, 'manager_id' => null, 'location_id' => 1],
                ['id' => 2, 'name' => 'Fleet Manager - 1', 'salary' => 15000, 'rating' => 4, 'role_id' => 2, 'manager_id' => 1, 'location_id' => 1],
                ['id' => 3, 'name' => 'Fleet Manager - 2', 'salary' => 15000, 'rating' => 4, 'role_id' =>2, 'manager_id' => 1, 'location_id' => 1],
                ['id' => 4, 'name' => 'Delivery Executive - 1', 'salary' => 10000, 'rating' => 4, 'role_id' => 3, 'manager_id' => 2, 'location_id' => 1],
                ['id' => 5, 'name' => 'Delivery Executive - 2', 'salary' => 9000, 'rating' => 4, 'role_id' => 3, 'manager_id' => 2, 'location_id' => 1],
                ['id' => 6, 'name' => 'Delivery Executive - 3', 'salary' => 8000, 'rating' => 3, 'role_id' => 3, 'manager_id' => 3, 'location_id' => 1],
                ['id' => 7, 'name' => 'Delivery Executive - 4', 'salary' => 10000, 'rating' => 3, 'role_id' => 3, 'manager_id' => 3, 'location_id' => 1],
                ['id' => 8, 'name' => 'Delivery Executive - 5', 'salary' => 7000, 'rating' => 4, 'role_id' => 3, 'manager_id' => 3, 'location_id' => 1],
            ]);

    }
}
