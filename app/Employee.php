<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'salary', 'rating', 'role_id', 'location_id', 'manager_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function location() {
        return $this->belongsTo('App\Location');
    }

    public function manager()
    {
        return $this->hasOne('App\Employee', 'id', 'manager_id');
    }

    public function mentee()
    {
        return $this->hasMany('App\Employee', 'manager_id');
    }
}
