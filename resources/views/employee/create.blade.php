@extends('main')
@section('content')
    @if (isset($roles))
        <div class="text-center" >
            <h3>Create Employee</h3>

            @foreach($roles as $role)
                <p>Create - <a href="/employee/create?roleId={{ $role->id }}">{{ $role->name }}</a></p>
            @endforeach
        </div>
    @else:
        <form class="text-center" action="/employee/store" method="post">
            {{ csrf_field() }}
            <b>Adding for Role ({{ $requestedRole }} )</b>
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" placeholder="Enter Name" required/>
            </div>
            <div class="form-group">
                <label>Salary</label>
                <input type="number" name="salary" placeholder="Enter Salary" step="1" required/>
            </div>
            <div class="form-group">
                <label>Rating</label>
                <input style="width: 150px" type="number" name="rating" placeholder="Enter Rating" min="1" max="5" required/>
            </div>
            <div class="form-group">
                <input type="hidden" name="role_id" value="{{ $roleId }}" required/>
            </div>
            @if ($managerRole)
                <div class="form-group">
                    <label>Manager ({{$managerRole }})</label>
                    @if (!empty($managersList))
                        <select name="manager_id">
                            @foreach($managersList as $manager)
                                <option value="{{$manager->id}}">{{ $manager->name }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="location_id" value="{{ $manager->location_id }}">
                    @else
                        <div class="alert-warning"> Please add {{$managerRole }} in the System first before adding this.</div>
                    @endif
                </div>
            @else
                <div class="form-group">
                    <label>Location</label>
                    <select name="location_id">
                        @foreach($locations as $location)
                            <option value="{{$location->id}}">{{ $location->name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            @if (!$managerRole or !empty($managersList))
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            @endif
        </form>
    @endif
@endsection