@extends('main')
@section('content')
    <h3 class="text-center">List of all employees</h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Rating</th>
                <th scope="col">Salary</th>
                <th scope="col">Role</th>
                <th scope="col">Hierarchy</th>
            </tr>
        </thead>
        <tbody>
            @foreach($employees as $emp)
                <tr>
                    <td>{{ $emp->id }}</td>
                    <td>{{ $emp->name }}</td>
                    <td>{{ $emp->rating }}</td>
                    <td>{{ $emp->salary }}</td>
                    <td>{{ $emp->role->name }}</td>
                    <td><a href="{{ '/employee/hierarchy/'. $emp->id }}">Show Hierarchy</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection