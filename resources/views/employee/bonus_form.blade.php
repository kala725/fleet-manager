@extends('main')
@section('content')
    <form class="text-center" action="/employee/bonus" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Location</label>
            <select name="location_id" required>
                @foreach($locations as $location)
                    <option value="{{$location->id}}">{{ $location->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Amount</label>
            <input type="text" name="amount" placeholder="Enter Bonus Amount" required/>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
@endsection