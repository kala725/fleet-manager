<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';

    public function employees()
    {
        return $this->hasMany('App\Employee');
    }

    public function parent()
    {
        return $this->hasOne('App\Location', 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasOne('App\Location', 'parent_id');
    }
}
