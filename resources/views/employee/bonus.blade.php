@extends('main')
@section('style')
    <style>td { white-space:pre }</style>
@endsection
@section('content')
    <h3>Bonus: </h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Role</th>
            <th scope="col">Rating</th>
            <th scope="col">Bonus</th>
            <th scope="col">Salary</th>
            <th scope="col">Bonus Salary Ratio<span class="glyphicon glyphicon-sort"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($mentees as $empId => $mentee)
            <tr>
                <td scope="col">{{ $mentee['name'] }}</td>
                <td scope="col">{{ $mentee['role'] }}</td>
                <td scope="col">{{ $mentee['rating'] }}</td>
                <td scope="col">{{ $mentee['bonus'] }}</td>
                <td scope="col">{{ $mentee['salary'] }}</td>
                <td scope="col">{{ round($mentee['bonus_salary_ratio'],3) . ": 1 \n" . (round($mentee['bonus_salary_ratio'],3) * 100) . '%' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection