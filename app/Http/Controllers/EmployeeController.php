<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Location;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class EmployeeController extends Controller
{
    /**
     * @param Request $request
     *
     * Shows all the employees with their respective details and a option to show hierarchy
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function index(Request $request)
    {
        $employees = Employee::with('role')->get();
        return view('employee.index', ['employees' => $employees]);
    }

    /**
     * Get the employee hierarchy given employee id upto the top most level.
     * @param Request $request
     *
     * @param $employeeId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getHierarchy(Request $request, $employeeId)
    {
        $employee = Employee::findOrFail($employeeId);
        $hierarchy[] = ['name' => $employee->name, 'role' => $employee->role->name ];

        while ($employee->manager) {
            $hierarchy[] = ['name' => $employee->manager->name, 'role' => $employee->manager->role->name ];
            $employee = $employee->manager;
        }
        return view('employee/hierarchy', ['hierarchy' => $hierarchy]);
    }

    /**
     *
     * Disburse the bonus among employees in respect to their ratings.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bonus(Request $request)
    {
        if ($request->method() == 'GET') {
            $locations = Location::all();
            return view('employee.bonus_form', ['locations' => $locations]);
        }
        $this->validate($request, [
            'location_id' => 'int|required',
            'amount' => 'int|required'
        ]);
        $data = $request->post();

        $employee = Employee::where('location_id', $data['location_id'])
            ->where('manager_id', null)
            ->first();

        if (!$employee or $employee->count() === 0) {
            dd('No employee there for this city');
        }

        $mentees = $this->getBottomLevelMentees($employee, []);

        $ratings = Arr::pluck($mentees, 'rating');

        $perRatingAmount = $data['amount'] / array_sum($ratings);

        foreach ($mentees as $key => &$mentee) {
            $mentee['bonus'] = round($mentee['rating'] * $perRatingAmount, 2);
            $mentee['bonus_salary_ratio'] = $mentee['bonus'] / $mentee['salary'];
        }

        $sortedArr = Arr::sort($mentees, function($value) {
            return [$value['bonus_salary_ratio'], $value['salary']];
        });

        return view('employee/bonus', ['mentees' => array_reverse($sortedArr)]);
    }

    /**
     * Recursively get all the bottom level employees who doesnt have any mentee(who is not a manager).
     *
     * @param $employee
     * @param $mentees
     * @return mixed
     */
    private function getBottomLevelMentees($employee, $mentees)
    {
        if ($employee->mentee->count() == 0) {
            $mentees[$employee->id] = ['name' => $employee->name,
                'rating' => $employee->rating,
                'salary' => $employee->salary,
                'role' => $employee->role->name,
            ];
            return $mentees;
        }

        foreach ($employee->mentee as $emp) {
            $mentees = $this->getBottomLevelMentees($emp, $mentees);
        }
        return $mentees;
    }

    /**
     * Creates the employee given details.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'string|required',
            'salary' => 'int|required',
            'rating' => 'int|required|min:1|max:5',
            'role_id' => 'int|required|min:1|max:5',
            'location_id' => 'int|required',
            'manager_id' => 'int|nullable',
        ]);

        $data = $request->post();

        if (Employee::create($data)) {
            return redirect('employee');
        }
    }

    /**
     * Renders the Page to create employees based on their roles.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $roleId = $request->get('roleId', null);

        if (null === $roleId) {
            // Let's choose a role first
            $roles = Role::all();
            return view('employee.create', ['roles' => $roles]);
        }

        $role = Role::find($roleId);

        $managersRole = $role->parent;
        $locations = Location::all();
        $managersList = null;

        if ($managersRole) {
            $managersList = Employee::where('role_id', $managersRole->id)->get();
            if ($managersList->count() === 0) {
                $managersList = null;
            }
        }

        return view('employee.create', [
            'managersList' => $managersList,
            'locations' => $locations,
            'roleId' => $roleId,
            'requestedRole' => $role->name,
            'managerRole' => $managersRole ? $managersRole->name : null
        ]);
    }
}
