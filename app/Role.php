<?php

namespace App;

use \Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    public function employees()
    {
        return $this->hasMany('App\Employee', 'role_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne('App\Role', 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasOne('App\Role', 'parent_id');
    }
}
