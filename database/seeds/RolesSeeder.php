<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('roles')
            ->insert([
                ['id'=> 1, 'name' => 'City Manager', 'slug' => 'CM', 'parent_id' => null],
                ['id'=> 2, 'name' => 'Fleet Manager', 'slug' => 'FM', 'parent_id'=> 1],
                ['id'=> 3, 'name' => 'Delivery Executives',  'slug' => 'DE', 'parent_id'=>2]
            ]);
    }
}
